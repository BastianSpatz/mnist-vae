import torch
import torch.nn as nn


class VAE(nn.Module):
    """
    Variational Autoencoder
    Args:
        input_size (int):
        latent_dim (int): size of the latent space dimension.
    """

    def __init__(self, latent_dim) -> None:
        super(VAE, self).__init__()

        self.encoder = Encoder(latent_dim)
        self.decoder = Decoder(latent_dim)

    def reparameterize(self, mu, logvar):
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return mu + eps * std

    def forward(self, x):
        mu, logvar = self.encoder(x)
        z = self.reparameterize(mu, logvar)
        x = self.decoder(z)
        return x, mu, logvar


class Encoder(nn.Module):
    """
    Encoder
    Args:
        input_size (int):
        latent_dim (int): size of the latent space dimension.
    """

    def __init__(self, latent_dim) -> None:
        super(Encoder, self).__init__()

        self.flatten = nn.Flatten(start_dim=1)
        self.fc1 = nn.Linear(28 * 28, 512)
        self.relu = nn.ReLU()
        self.fc21 = nn.Linear(512, latent_dim)
        self.fc22 = nn.Linear(512, latent_dim)

    def forward(self, x):
        x = self.flatten(x)
        h = self.relu(self.fc1(x))
        return self.fc21(h), self.fc22(h)


class Decoder(nn.Module):
    """
    Decoder
    Args:
        input_size (int):
        latent_dim (int): size of the latent space dimension.
    """

    def __init__(self, latent_dim) -> None:
        super(Decoder, self).__init__()

        self.decoder = nn.Sequential(
            nn.Linear(latent_dim, 512),
            nn.ReLU(),
            nn.Linear(512, 28 * 28),
            nn.Sigmoid(),
        )

    def forward(self, z):
        x = self.decoder(z)
        return x
